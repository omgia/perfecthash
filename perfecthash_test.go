package perfecthash_test

import (
	"fmt"
	"math/rand"
	"perfecthash"
	"testing"
	"time"
)

var testSetSize = 1000000

type myNum struct {
	num uint64
}

func (num myNum) ToUint64() uint64 {
	return num.num
}

func TestPerfectSet(t *testing.T) {
	seed := rand.NewSource(time.Now().UnixNano())
	rnd := rand.New(seed)

	fmt.Println("Testing hash set code", time.Now())

	testSet := make([]perfecthash.Key, testSetSize)
	for {
		for i := 0; i < testSetSize; i++ {
			testSet[i] = myNum{rnd.Uint64()}
		}

		if perfecthash.VerifyUniqueness(testSet) {
			break
		} else {
			fmt.Println("Set contains duplicated values, regenrating values.")
		}
	}

	set, err := perfecthash.BuildSet(testSet)
	if err != nil {
		fmt.Println(err)
		t.Error("Failed to create hash set")
	}
	for i := 0; i < testSetSize; i++ {
		if !set.Exist(testSet[i]) {
			t.Error("Failed to find", testSet[i], "in the set!")
		}
	}

	num := rnd.Uint64()
	isInSet := false
	for i := 0; i < testSetSize; i++ {
		if num == testSet[i].ToUint64() {
			isInSet = true
			break
		}
	}
	if set.Exist(myNum{num}) != isInSet {
		t.Error("Expected set.Exist(num) ==", isInSet)
	}

	testSet[testSetSize-1] = testSet[0]
	if perfecthash.VerifyUniqueness(testSet) {
		t.Error("Expected perfecthash.VerifyUniqueness(testSet) to fail")
	}

	fmt.Println("Successfuly completed hash set integrity test", time.Now())

	fmt.Println("Testing hash map code", time.Now())

	testMapKeys := make([]perfecthash.Key, testSetSize)
	testMapValues := make([]perfecthash.Value, testSetSize)
	for i := 0; i < testSetSize; i++ {
		testMapKeys[i] = myNum{rnd.Uint64()}
		testMapValues[i] = rnd.Uint64()
	}

	hMap, err := perfecthash.BuildMap(testMapKeys, testMapValues)
	if err != nil {
		fmt.Println(err)
		t.Error("Failed to create hash map")
	}

	for i := 0; i < testSetSize; i++ {
		v, err := hMap.Get(testMapKeys[i])
		if err != nil {
			fmt.Println(err)
			t.Error("Failed to retrieve value for a valid key")
		}
		if (*v).(uint64) != testMapValues[i].(uint64) {
			t.Error("Retrieved a wrong value for a valid key")
		}
	}
	fmt.Println("Successfuly completed hash map integrity test", time.Now())

}
