package perfecthash

import (
	"container/list"
	"fmt"
	"math/rand"
	"runtime"
	"time"
)

// mulModi - A pair made out of multiplier and modifier
type mulModi struct {
	Multiplier uint64
	Modifier   uint64
}

// HashSet - A hashed set of keys (without associated values)
type HashSet struct {
	leftSet  []uint64
	rightSet []uint64
	left     mulModi
	right    mulModi
	mask     uint64
}

// HashMap - A hashed map of keys with associated values
type HashMap struct {
	HashSet
	leftIdx  []uint64
	rightIdx []uint64
	values   []Value
}

type edge struct {
	key         uint64
	left        uint64
	right       uint64
	rightSource bool
	mark        bool
}

type indexedEdge struct {
	edge
	index uint64
}

type node struct {
	mark  bool
	edges []*edge
}

type indexedNode struct {
	mark  bool
	edges []*indexedEdge
}

// Key - The hash key should prtovide a method for uint64 conversion
type Key interface {
	ToUint64() uint64
}

// Value - An empty interface
type Value interface {
}

var htNumOfKeys = []int{8, 16, 24, 36, 54, 78, 112, 160}
var htSize = []int{8, 16, 32, 64, 128, 256, 512, 1024}

// BuildSet - Build hashed set from a given slice of keys
func BuildSet(keys []Key) (*HashSet, error) {
	if len(keys) == 0 {
		return nil, fmt.Errorf("Number of keys is zero")
	}

	k := make([]uint64, len(keys))
	for i := 0; i < len(keys); i++ {
		k[i] = keys[i].ToUint64()
	}

	if len(k) <= htNumOfKeys[len(htNumOfKeys)-1] {
		return bruteForceHashSet(k)
	}

	return smartHashSet(k)
}

// Exist - Returns true if 'key' is in 'set'
func (set *HashSet) Exist(key Key) bool {
	k := key.ToUint64()
	if set.left.Multiplier == 0 {
		return false
	}
	if set.right.Multiplier == 0 {
		return k == set.leftSet[calcHash(k, set.left, set.mask)]
	}
	return k == (set.leftSet[calcHash(k, set.left, set.mask)] ^ set.rightSet[calcHash(k, set.right, set.mask)])
}

// BuildMap - Build hashed map from the given slices of keys and values
func BuildMap(keys []Key, values []Value) (*HashMap, error) {
	if len(keys) != len(values) {
		return nil, fmt.Errorf("Number of keys (%d) doesn't match number of values (%d)", len(keys), len(values))
	}

	if len(keys) == 0 {
		return nil, fmt.Errorf("Number of key <-> value pairs is zero")
	}
	if len(keys) <= htNumOfKeys[len(htNumOfKeys)-1] {
		return bruteForceHashMap(keys, values)
	}

	return smartHashMap(keys, values)
}

// Get - Returns the value associated with the given key
func (hashMap *HashMap) Get(key Key) (*Value, error) {
	if hashMap.left.Multiplier == 0 {
		return nil, fmt.Errorf("HashMap is not properly initialized")
	}
	if hashMap.right.Multiplier == 0 {
		idx := calcHash(key.ToUint64(), hashMap.left, hashMap.mask)
		success := key.ToUint64() == hashMap.leftSet[idx]
		if success {
			return &hashMap.values[hashMap.leftIdx[idx]], nil
		}
		return nil, fmt.Errorf("Failed to find key. HashMap configured by brute force")
	}
	leftIdx := calcHash(key.ToUint64(), hashMap.left, hashMap.mask)
	rightIdx := calcHash(key.ToUint64(), hashMap.right, hashMap.mask)
	success := key.ToUint64() == (hashMap.leftSet[leftIdx] ^ hashMap.rightSet[rightIdx])
	if success {
		return &hashMap.values[hashMap.leftIdx[leftIdx]^hashMap.rightIdx[rightIdx]], nil
	}
	return nil, fmt.Errorf("failed to find key")
}

func calcHash(key uint64, mm mulModi, mask uint64) uint64 {
	//	tempRes := (key - 0xDEADBEEFDEADBEEF) * multiplier
	tempRes := (key ^ mm.Modifier) * mm.Multiplier
	res := (tempRes >> 56) |
		((tempRes & 0x00FF000000000000) >> 40) |
		((tempRes & 0x0000FF0000000000) >> 24) |
		((tempRes & 0x000000FF00000000) >> 8) |
		((tempRes & 0x00000000FF000000) << 8) |
		((tempRes & 0x0000000000FF0000) << 24) |
		((tempRes & 0x000000000000FF00) << 40) |
		((tempRes & 0x00000000000000FF) << 56)

	return (res & mask)
}

func bruteForceHashSet(keys []uint64) (*HashSet, error) {
	result := make(chan mulModi, 1024)
	done := make(chan uint8, 1024)

	numOfCores := runtime.NumCPU()

	var mask uint64 = 0x7
	for i := 0; i < len(htNumOfKeys); i++ {
		if len(keys) <= htNumOfKeys[i] {
			break
		}
		mask <<= 1
		mask |= 1
	}
	for i := 0; i < numOfCores; i++ {
		go bruteForceHashFunctionSearcher(keys, result, done, mask, i)
	}

	multMod := <-result
	result <- multMod

	count := 0
	for {
		x := <-done
		count += int(x)
		if count == numOfCores {
			break
		}
	}

	close(result)
	close(done)

	var res HashSet
	res.leftSet = make([]uint64, mask+1)
	res.left = multMod
	res.mask = mask

	for i := 0; i < len(keys); i++ {
		res.leftSet[calcHash(keys[i], multMod, mask)] = keys[i]
	}

	return &res, nil

}

func bruteForceHashMap(k []Key, values []Value) (*HashMap, error) {
	result := make(chan mulModi, 1024)
	done := make(chan uint8, 1024)
	keys := make([]uint64, len(k))
	for i := 0; i < len(k); i++ {
		keys[i] = k[i].ToUint64()
	}
	numOfCores := runtime.NumCPU()

	var mask uint64 = 0x7
	for i := 0; i < len(htNumOfKeys); i++ {
		if len(keys) <= htNumOfKeys[i] {
			break
		}
		mask <<= 1
		mask |= 1
	}
	for i := 0; i < numOfCores; i++ {
		go bruteForceHashFunctionSearcher(keys, result, done, mask, i)
	}

	multMod := <-result
	result <- multMod

	count := 0
	for {
		x := <-done
		count += int(x)
		if count == numOfCores {
			break
		}
	}

	close(result)
	close(done)

	var res HashMap
	res.leftSet = make([]uint64, mask+1)
	res.values = make([]Value, mask+1)
	res.leftIdx = make([]uint64, mask+1)
	res.left = multMod
	res.mask = mask

	for i := 0; i < len(keys); i++ {
		idx := calcHash(keys[i], multMod, mask)
		res.leftSet[idx] = keys[i]
		res.values[i] = values[i]
		res.leftIdx[idx] = uint64(i)
	}

	return &res, nil

}

func bruteForceHashFunctionSearcher(values []uint64, result chan mulModi, done chan uint8, mask uint64, id int) {
	seed := rand.NewSource(time.Now().UnixNano() + int64(id))
	rnd := rand.New(seed)
	stop := false

	for {
		select {
		case cntl := <-result:
			result <- cntl
			done <- 1
			stop = true

		default:
			for i := 0; i < 1024; i++ {
				var mm = mulModi{rnd.Uint64(), rnd.Uint64()}
				btHt := make([]uint8, htSize[len(htSize)-1])
				success := true
				for j := 0; j < len(values); j++ {
					idx := calcHash(values[j], mm, mask)
					if btHt[idx] == 0 {
						btHt[idx] = 1
					} else {
						success = false
						break
					}
				}
				if success {
					result <- mm
					done <- 1
					stop = true
					break
				}
			}
		}
		if stop {
			break
		}
	}
}

func smartHashSet(keys []uint64) (*HashSet, error) {
	result := make(chan *HashSet, 1024)
	done := make(chan uint8, 1024)

	numOfCores := runtime.NumCPU() / 2
	if numOfCores == 0 {
		numOfCores = 1
	}

	stop := false
	for i := 0; i < numOfCores; i++ {
		go smartHashSearcher(keys, result, done, i, &stop)
	}

	res := <-result
	fillSetValues(res, keys)
	for i := 0; i < numOfCores; i++ {
		<-done
	}

	close(result)
	close(done)

	return res, nil
}

func smartHashMap(k []Key, values []Value) (*HashMap, error) {
	result := make(chan *HashSet, 1024)
	done := make(chan uint8, 1024)
	keys := make([]uint64, len(k))
	for i := 0; i < len(k); i++ {
		keys[i] = k[i].ToUint64()
	}

	numOfCores := runtime.NumCPU() / 2
	if numOfCores == 0 {
		numOfCores = 1
	}

	stop := false
	for i := 0; i < numOfCores; i++ {
		go smartHashSearcher(keys, result, done, i, &stop)
	}

	setRes := <-result
	var res HashMap
	fillMapValues(&res, setRes, keys, values)
	for i := 0; i < numOfCores; i++ {
		<-done
	}

	close(result)
	close(done)

	return &res, nil
}

func smartHashSearcher(values []uint64, result chan *HashSet, done chan uint8, id int, stop *bool) {
	seed := rand.NewSource(time.Now().UnixNano() + int64(id*17))
	rnd := rand.New(seed)
	mask := uint64(2)
	for ; mask < uint64(len(values))+1; mask <<= 1 {
	}
	mask--

	for {
		var res HashSet
		res.left.Multiplier = rnd.Uint64()
		res.left.Modifier = rnd.Uint64()
		res.right.Multiplier = rnd.Uint64()
		res.right.Modifier = rnd.Uint64()
		res.mask = mask
		res.leftSet = make([]uint64, res.mask+1)
		res.rightSet = make([]uint64, res.mask+1)

		left := make([]node, res.mask+1)
		right := make([]node, res.mask+1)

		for i := 0; i < len(values); i++ {
			if *stop {
				done <- 1
				return
			}
			leftIndex := calcHash(values[i], res.left, res.mask)
			rightIndex := calcHash(values[i], res.right, res.mask)

			e := edge{values[i], leftIndex, rightIndex, false, false}
			left[leftIndex].edges = append(left[leftIndex].edges, &e)
			right[rightIndex].edges = append(right[rightIndex].edges, &e)
		}
		success, sendRes := isAcyclic(left, right, stop)
		if success {
			if sendRes {
				*stop = true
				result <- &res
			}
			done <- 1
			return
		}
	}
}

func isAcyclic(left []node, right []node, stop *bool) (bool, bool) {
	edges := list.New()

	for i := 0; i < len(left); i++ {
		if *stop {
			return true, false
		}

		if left[i].mark == false && len(left[i].edges) > 0 && (len(left[i].edges) > 1 || len(right[left[i].edges[0].right].edges) > 1) {
			for j := 0; j < len(left[i].edges); j++ {
				left[i].edges[j].mark = true
				if right[left[i].edges[j].right].mark == false {
					right[left[i].edges[j].right].mark = true
				} else {
					return false, false
				}
				edges.PushBack(left[i].edges[j])
			}
			left[i].mark = true

			for edges.Len() > 0 {
				curEdge := edges.Front()
				e := curEdge.Value.(*edge)

				if e.rightSource {
					for k := 0; k < len(left[e.left].edges); k++ {
						if left[e.left].edges[k].mark == false {
							if right[left[e.left].edges[k].right].mark == true {
								return false, false
							}
							right[left[e.left].edges[k].right].mark = true
							left[e.left].edges[k].mark = true
							edges.PushBack(left[e.left].edges[k])
						}
					}
				} else {
					for k := 0; k < len(right[e.right].edges); k++ {
						if right[e.right].edges[k].mark == false {
							if left[right[e.right].edges[k].left].mark == true {
								return false, false
							}
							left[right[e.right].edges[k].left].mark = true
							right[e.right].edges[k].mark = true
							right[e.right].edges[k].rightSource = true
							edges.PushBack(right[e.right].edges[k])
						}
					}
				}
				edges.Remove(curEdge)
			}
		}
	}
	return true, true
}

func fillSetValues(res *HashSet, values []uint64) {
	seed := rand.NewSource(time.Now().UnixNano())
	rnd := rand.New(seed)
	left := make([]node, res.mask+1)
	right := make([]node, res.mask+1)
	edges := list.New()
	for i := 0; i < len(values); i++ {
		leftIndex := calcHash(values[i], res.left, res.mask)
		rightIndex := calcHash(values[i], res.right, res.mask)

		e := edge{values[i], leftIndex, rightIndex, false, false}
		left[leftIndex].edges = append(left[leftIndex].edges, &e)
		right[rightIndex].edges = append(right[rightIndex].edges, &e)
	}

	for i := 0; i < len(left); i++ {
		if left[i].mark == false && len(left[i].edges) > 0 {
			for j := 0; j < len(left[i].edges); j++ {
				left[i].edges[j].mark = true
				right[left[i].edges[j].right].mark = true
				edges.PushBack(left[i].edges[j])
				if res.leftSet[i] == 0 {
					res.leftSet[i] = rnd.Uint64()
				}
			}
			left[i].mark = true

			for edges.Len() > 0 {
				curEdge := edges.Front()
				e := curEdge.Value.(*edge)
				if e.rightSource {
					res.leftSet[e.left] = res.rightSet[e.right] ^ e.key
					for k := 0; k < len(left[e.left].edges); k++ {
						if left[e.left].edges[k].mark == false {
							right[left[e.left].edges[k].right].mark = true
							left[e.left].edges[k].mark = true
							edges.PushBack(left[e.left].edges[k])
						}
					}
				} else {
					res.rightSet[e.right] = res.leftSet[e.left] ^ e.key
					for k := 0; k < len(right[e.right].edges); k++ {
						if right[e.right].edges[k].mark == false {
							left[right[e.right].edges[k].left].mark = true
							right[e.right].edges[k].mark = true
							right[e.right].edges[k].rightSource = true
							edges.PushBack(right[e.right].edges[k])
						}
					}
				}
				edges.Remove(curEdge)
			}
		}
	}
}

func fillMapValues(mapRes *HashMap, setRes *HashSet, keys []uint64, values []Value) {
	mapRes.leftSet = setRes.leftSet
	mapRes.rightSet = setRes.rightSet
	mapRes.left = setRes.left
	mapRes.right = setRes.right
	mapRes.mask = setRes.mask
	mapRes.values = values
	mapRes.leftIdx = make([]uint64, mapRes.mask+1)
	mapRes.rightIdx = make([]uint64, mapRes.mask+1)

	seed := rand.NewSource(time.Now().UnixNano())
	rnd := rand.New(seed)
	left := make([]indexedNode, mapRes.mask+1)
	right := make([]indexedNode, mapRes.mask+1)
	edges := list.New()
	for i := 0; i < len(keys); i++ {
		leftIndex := calcHash(keys[i], mapRes.left, mapRes.mask)
		rightIndex := calcHash(keys[i], mapRes.right, mapRes.mask)

		e := indexedEdge{edge{keys[i], leftIndex, rightIndex, false, false}, uint64(i)}
		left[leftIndex].edges = append(left[leftIndex].edges, &e)
		right[rightIndex].edges = append(right[rightIndex].edges, &e)
	}

	for i := 0; i < len(left); i++ {
		if left[i].mark == false && len(left[i].edges) > 0 {
			for j := 0; j < len(left[i].edges); j++ {
				left[i].edges[j].mark = true
				right[left[i].edges[j].right].mark = true
				edges.PushBack(left[i].edges[j])
				if mapRes.leftSet[i] == 0 {
					mapRes.leftSet[i] = rnd.Uint64()
					mapRes.leftIdx[i] = rnd.Uint64()
				}
			}
			left[i].mark = true

			for edges.Len() > 0 {
				curEdge := edges.Front()
				e := curEdge.Value.(*indexedEdge)
				if e.rightSource {
					mapRes.leftSet[e.left] = mapRes.rightSet[e.right] ^ e.key
					mapRes.leftIdx[e.left] = mapRes.rightIdx[e.right] ^ e.index
					for k := 0; k < len(left[e.left].edges); k++ {
						if left[e.left].edges[k].mark == false {
							right[left[e.left].edges[k].right].mark = true
							left[e.left].edges[k].mark = true
							edges.PushBack(left[e.left].edges[k])
						}
					}
				} else {
					mapRes.rightSet[e.right] = mapRes.leftSet[e.left] ^ e.key
					mapRes.rightIdx[e.right] = mapRes.leftIdx[e.left] ^ e.index
					for k := 0; k < len(right[e.right].edges); k++ {
						if right[e.right].edges[k].mark == false {
							left[right[e.right].edges[k].left].mark = true
							right[e.right].edges[k].mark = true
							right[e.right].edges[k].rightSource = true
							edges.PushBack(right[e.right].edges[k])
						}
					}
				}
				edges.Remove(curEdge)
			}
		}
	}
}

// VerifyUniqueness - Verify that there are no duplicated values
func VerifyUniqueness(keys []Key) bool {
	seed := rand.NewSource(time.Now().UnixNano())
	rnd := rand.New(seed)
	mask := uint64(16)

	k := make([]uint64, len(keys))
	for i := 0; i < len(keys); i++ {
		k[i] = keys[i].ToUint64()
	}
	for mask < uint64(len(k))*8 {
		mask <<= 1
	}
	mask--

	var hash = mulModi{rnd.Uint64(), rnd.Uint64()}
	ht := make([]uint64, mask+1)

	for i := 0; i < len(k); i++ {
		prevIndex := uint64(0)
		prevShift := uint64(0)
		for {
			index := calcHash(k[i]+prevIndex+prevShift, hash, mask)
			if ht[index] == 0 {
				ht[index] = k[i]
				break
			} else if ht[index] == k[i] {
				return false
			}
			prevIndex = index
			prevShift++
		}
	}
	return true
}
